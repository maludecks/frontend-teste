$(function(){
    $('.required').on('blur', function(e) {
        validate_input(e.target)
    });

    $('input[name="telefone"]').mask('(99) 9999-9999?9');
});

function validate_input(input)
{
    var form = $(input).parents('form');

    $(input).removeClass('error-input');

    if ($(input).val() == '' || $(input).val() == null)
    {
        $(input).addClass('error-input');
    }

    enable_submit_button(form);
}

function validate_password(el)
{
    var form = $(el).parents('form');
    var password = $(form).find('input[name="senha"]');
    var confirm  = $(form).find('input[name="senha_confirme"]');

    $(form).find('#password-reminder').addClass('hide');

    var password_val = $(password).val();
    var confirm_val = $(confirm).val();

    if ((password_val != '' && confirm_val != '') && (password_val != confirm_val))
    {
        $(form).find('#password-reminder').removeClass('hide');

        password.val('');
        confirm.val('');

        enable_submit_button(form);
    }
}

function enable_submit_button(form)
{
    var required_fields = $(form).find('input.required');
    var err = false;

    $(form).find('#reminder').removeClass('hide');
    $(form).find('#submit').attr('disabled', true);

    $.each(required_fields, function(i, input) {
        if ($(input).val() == '' || $(input).val() == null)
        {
            err = true;
        }
    });

    if (!err)
    {
        $(form).find('#submit').attr('disabled', false);
        $(form).find('#reminder').addClass('hide');
    }
}