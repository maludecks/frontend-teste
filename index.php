<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Eleve CRM</title>

        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="/frontend-teste/assets/css/main.min.css">
    </head>

    <body>
        <header class="container-fluid">

            <div class="row">
                <div id="logo-holder" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                    <img alt="Eleve CRM" title="Eleve CRM" src="/frontend-teste/assets/img/logo-eleve.png">
                </div>
            </div>

            <div class="row">
                <div id="main-info-holder" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h1 class="text-center">Ótima escolha!</h1>

                    <div class="subtitle">
                        <h2 class="text-center">Crie sua conta <strong>gratuita</strong> e amplie seus resultados.</h2>
                        <h2 class="text-center">EleveCRM é a forma mais eficaz de elevar os resultados de seu time comercial!</h2>
                    </div>
                </div>
            </div>

            <div id="sub-info-holder" class="row">
                <div id="mkt-holder" class="col-md-6 col-lg-6 col-sm-6 hidden-xs">
                    <img class="img-responsive" alt="Plataforma Eleve" title="Plataforma Eleve" src="/frontend-teste/assets/img/image.png">
                </div>
                <div class="col-md-4 col-lg-4 col-sm-5 hidden-xs">
                    <div id="form-holder">
                        <form method="POST" class="form-contato">
                            <fieldset>
                                <div class="form-group">
                                    <label>Dados pessoais</label>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="nome" class="form-control required" placeholder="Nome">
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control required" placeholder="E-mail">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="senha" class="form-control required" placeholder="Senha" onblur="validate_password(this);">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="senha_confirme" class="form-control required" placeholder="Confirmação de senha" onblur="validate_password(this);">
                                </div>
                                <small id="password-reminder" class="text-muted hide">A senha e a confirmação devem ser iguais</small>
                            </fieldset>

                            <fieldset>
                                <div class="form-group">
                                    <label>Dados da empresa</label>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="cnpj" class="form-control required" placeholder="CNPJ">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="razao_social required" class="form-control" placeholder="Razão social">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="nome_fantasia" class="form-control" placeholder="Nome fantasia">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="site" class="form-control" placeholder="Site">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="telefone" class="form-control required" placeholder="Telefone">
                                </div>
                            </fieldset>

                            <div class="row" id="submit-opt">
                                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="termos"> Concordo com os <a href="#">termos de serviço</a>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                                    <button id="submit" class="btn btn-primary" disabled>Criar conta</button>
                                    <small id="reminder" class="text-muted hide">Atenção aos campos obrigatórios</small>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </header>

        <section id="form-container" class="container-fluid hidden-sm hidden-md hidden-lg">
            <div id="form-mobile-holder" class="row">
                <form method="POST" class="form-contato">
                    <fieldset>
                        <div class="form-group">
                            <label>Dados pessoais</label>
                        </div>
                        <div class="form-group">
                            <input type="text" name="nome" class="form-control required" placeholder="Nome">
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control required" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <input type="password" name="senha" class="form-control required" placeholder="Senha" onblur="validate_password(this);">
                        </div>
                        <div class="form-group">
                            <input type="password" name="senha_confirme" class="form-control required" placeholder="Confirmação de senha" onblur="validate_password(this);">
                        </div>
                        <small id="password-reminder" class="text-muted hide">A senha e a confirmação devem ser iguais</small>
                    </fieldset>

                    <fieldset>
                        <div class="form-group">
                            <label>Dados da empresa</label>
                        </div>
                        <div class="form-group">
                            <input type="text" name="cnpj" class="form-control required" placeholder="CNPJ">
                        </div>
                        <div class="form-group">
                            <input type="text" name="razao_social required" class="form-control" placeholder="Razão social">
                        </div>
                        <div class="form-group">
                            <input type="text" name="nome_fantasia" class="form-control" placeholder="Nome fantasia">
                        </div>
                        <div class="form-group">
                            <input type="text" name="site" class="form-control" placeholder="Site">
                        </div>
                        <div class="form-group">
                            <input type="text" name="telefone" class="form-control required" placeholder="Telefone">
                        </div>
                    </fieldset>

                    <div class="row" id="submit-opt">
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="termos"> Concordo com os <a href="#">termos de serviço</a>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                            <button id="submit" class="btn btn-primary" disabled>Criar conta</button>
                            <small id="reminder" class="text-muted hide">Atenção aos campos obrigatórios</small>
                        </div>
                    </div>
                </form>
            </div>
        </section>

        <section id="sub-container" class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-6 hidden-xs">
                    <div id="beta-info-holder">
                        <p>
                        Você está se cadastrando em nova versão beta, totalmente gratuita por tempo indeterminado.
                        Aproveite e veja como é fácil gerenciar suas vendas e seu time comercial usando o Eleve CRM.
                        </p>
                        <div class="divider"></div>
                    </div>

                </div>
            </div>

        </section>

        <footer class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-3 col-md-3">
                        <div class="box-header">
                            <h3>Mapa de Navegação</h3>
                        </div>
                        <div class="box-content">
                            <ul class="nav-list">
                                <li>
                                    <a href="#">Conheça</a>
                                </li>
                                <li>
                                    <a href="#">Planos</a>
                                </li>
                                <li>
                                    <a href="#">Blog</a>
                                </li>
                                <li>
                                    <a href="#">Sou cliente</a>
                                </li>
                                <li>
                                    <a href="#">Cadastre-se</a>
                                </li>
                                <li>
                                    <a href="#">Contato</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="box-header">
                            <h3>Sobre nós</h3>
                        </div>
                        <div class="box-content">
                            <p>
                                O ELEVEcrm foi concebido, objetivando acelerar o processo comercial,
                                integrar práticas e formalizar etapas de atendimento, permitindo formatar
                                e disponibilizar documentos, formulários e e-mails padrões, e acompanhar
                                todo o ciclo comercial, com leveza, simplicidade e muito gerenciamento
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-md-5">
                        <div class="box-header">
                            <h3>Últimas do blog</h3>
                        </div>
                        <div class="box-content">
                            <div class="blog-list-item">
                                <a href="#">O que seria Customer Success?</a>
                                <br>
                                <small class="text-muted">12/07/2017</small>
                            </div>
                            <div class="blog-list-item">
                                <a href="#">A boa entrega é a garantia da próxima venda!</a>
                                <br>
                                <small class="text-muted">12/07/2017</small>
                            </div>
                            <div class="blog-list-item last-item">
                                <a href="#">Afinal o que são vendas complexas?</a>
                                <br>
                                <small class="text-muted">12/07/2017</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-md-12 text-center">
                        <div id="logo-footer-holder">
                            <img src="/frontend-teste/assets/img/logo-footer.png" alt="Eleve CRM" title="Eleve CRM">
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </body>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="/frontend-teste/assets/js/main.min.js" type="text/javascript"></script>
    <script src="/frontend-teste/assets/js/jquery.maskedinput.min.js" type="text/javascript"></script>

</html>

